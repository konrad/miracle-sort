package main

import (
	"fmt"
	"golang.org/x/text/language"
	"golang.org/x/text/message"
	"golang.org/x/text/number"
	"sort"
	"time"
)

func main() {
	// This is the map we want to sort
	m := make(map[int]int)
	m[0] = 00
	m[1] = 11
	m[2] = 44
	m[3] = 33
	m[4] = 22
	m[5] = 55
	m[6] = 66
	m[7] = 77
	m[8] = 88
	m[9] = 99

	start := time.Now()
	iterations := 0
	var slice []int
	var sorted = false

	p := message.NewPrinter(language.English)

	for !sorted {
		// We put the map in a slice to have something more deterministic to work with
		slice = nil
		for _, msg := range m {
			slice = append(slice, msg)
		}

		// Check if the slice is sorted
		if sort.SliceIsSorted(slice, func(i, j int) bool {
			return slice[i] < slice[j]
		}) {
			sorted = true
			break
		}

		iterations++
		p.Printf("\rIteration %v, no miracle happened yet.", number.Decimal(iterations))
	}

	fmt.Println("\nSorted.")
	fmt.Printf("Took %v\n", time.Since(start))
	fmt.Printf("Iterations: %d\n", iterations)

	fmt.Println("Sorted slice:")
	for index, msg := range slice {
		fmt.Println(index, ": ", msg)
	}
}
